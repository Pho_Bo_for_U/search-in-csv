![](https://gitlab.com/Pho_Bo_for_U/search-in-csv/-/raw/main/photo_2024-05-30_20-35-59.jpg)


## Sources

* [How to find a particular word in a csv file in a particular column with pandas](https://stackoverflow.com/questions/70189925/how-to-find-a-particular-word-in-a-csv-file-in-a-particular-column-with-pandas)
* [CSV в Python](https://all-python.ru/osnovy/csv.html)
* [Почему не стоит использовать or для проверки нескольких условий в Python](https://nuancesprog.ru/p/14971/)
* [Как использовать Python для работы с CSV](https://sky.pro/media/kak-ispolzovat-python-dlya-raboty-s-csv/)
* [GitLab remote: HTTP Basic: Access denied and fatal Authentication](https://stackoverflow.com/questions/47860772/gitlab-remote-http-basic-access-denied-and-fatal-authentication)