import csv

seeklist = ['Netherlands', 'Russia', 'United Arab Emirates']

with open('world-cities.csv', encoding='utf-8', newline='') as csvfile1:
	reader = csv.DictReader(csvfile1)

	with open("output.csv", mode="w", encoding='utf-8') as w_file:
		names = ["name", "country", "subcountry", "geonameid"]
		file_writer = csv.DictWriter(w_file, delimiter = ",", lineterminator="\r", fieldnames=names)
		file_writer.writeheader()

		word_count = 0
		for row in reader:
			if row['country'] in seeklist:
			# if row['subcountry'] == 'St.-Petersburg':
				print(row)
				file_writer.writerow(row)
				word_count += 1

print(word_count)		